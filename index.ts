import { run } from "./lib/run";

import readline from "node:readline";
import { Config } from "./lib/Config";

function ask(query: string): Promise<string> {

	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout
	});

	return new Promise(resolve => rl.question(query, ans => {
		rl.close();
		resolve(ans);
	}));
}


const main = async () => {
	const filename = await ask("Choississez un filename\n");

	if (!filename) {
		console.error("veuillez choisir un filename");
		process.exit(1);
	}

	const check = await ask("Choississez un une chaine de caractere a recherche\n");

	const configParams = {
		filename,
		check
	};

	const config = new Config(configParams);

	run(config);
};

main();
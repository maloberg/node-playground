import { Config } from "./Config";

describe('config', () => {
	test('should have a length === 2', () => {
		const configParams = {
			filename: "bob.txt",
			check: "jose"
		};
		const config = new Config(configParams);

		if (!config) return;

		const [ content, errorWhileReading ] = config.readFile();

		if (!content) return;

		const [ filteredLines, errorWhileFiltering ] = config.filterLines(content);

		expect(filteredLines?.length).toBe(2);
	});


});

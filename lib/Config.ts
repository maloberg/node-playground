import fs from "node:fs";

export class Config {
	private filename: string;
	private check?: string;

	constructor({ filename, check }: { filename: string, check?: string; }) {
		this.filename = filename;
		this.check = check;
	}

	readFile(): string {
		try {
			const readedFile = fs.readFileSync(this.filename, { encoding: "utf-8" });
			return readedFile;

		} catch (error: any) {

			if (error[ "code" ] === "ENOENT") {
				throw new Error("Le fichier n'a pas ete trouve");
			}

			throw new Error("Une erreur est survenue");
		}
	}

	filterLines(content: string): string[] {
		const arrayOfLines = content.split("\n");

		if (!this.check) {
			throw new Error("vous n'avez pas selectionner de chaine de caracteres a trier");
		};

		const filteredLines = arrayOfLines.filter(element => (
			element
				.toLowerCase()
				.includes(this.check?.toLowerCase() as string)
		));

		return filteredLines;
	}
}
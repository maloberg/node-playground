import { Config } from "./Config";

export const run = (config: Config) => {
	let content: string;

	try {
		content = config.readFile();
	} catch (error) {
		console.error(error);
		process.exit(1);
	}

	if (!content) {
		console.error("une erreur est survenue");
		process.exit(1);
	}

	console.log(`le contenu du fichier recherche est ${content}`);

	let filteredLines: string[];

	try {
		filteredLines = config.filterLines(content);
	} catch (error) {
		console.error(error);
		process.exit(1);
	}

	if (!filteredLines?.length) {
		console.log("\naucune ligne ne contient le mot recherche");
		process.exit(1);
	}

	console.log("\nles mot trouves lors de la recherche sont\n");

	filteredLines.forEach(element => {
		console.log(element);
	});
};

